package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
)

func main() {
	sum := md5.Sum([]byte("The quick brown fox jumped over the lazy dog"))
	fmt.Println(hex.EncodeToString(sum[:]))
}
